//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2009 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

char oprel;
char current, lookedAhead;	// Current char	
int NLookedAhead=0;			

void ReadChar(void){
	if(NLookedAhead>0)
	{
		current = lookedAhead;		//Char has already been read
		NLookedAhead--;
	}
	else
		// Read character and skip spaces until 
		// non space character is read
	while(cin.get(current) && (current==' '||current=='\t'||current=='\n'));
}

void LookAhead(void)
{
	while(cin.get(lookedAhead) && (lookedAhead==' '||lookedAhead=='\t'||lookedAhead=='\n'))
		NLookedAhead++;
}

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}

// ArithmeticExpression := Term {AdditiveOperator Term}
// Term := Digit | "(" ArithmeticExpression ")"
// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" || "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | "<=" | ">=" | ">"
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"


	
void AdditiveOperator(void){
	if(current=='+'||current=='-'||current=='|'&&lookedAhead=='|')
	{
		ReadChar();
	}
	else
		Error("Opérateur additif attendu ou inconnu");	   // Additive operator expected
}

void RelationalOperator(void){
	if(current=='='||current=='<'||current=='>'||current=='<'&&lookedAhead=='='||current=='>'&&lookedAhead=='='||current=='!'&&lookedAhead=='=')
		ReadChar();
	else
		Error("Opérateur relationnel attendu ou inconnu");	//Relational operator expected
}

void MultiplicativeOperator(void){
	if(current=='*'||current=='/'||current=='%'||current=='&' && lookedAhead=='&')
	{
		ReadChar();
	}
	else
		Error("Opérateur multiplicatif attendu ou inconnu");	//Multiplicative operator expected
}

//Number:=Digit{Digit}

void Number(void)
{
	unsigned long long nombre;
	if((current<'0')||(current>'9'))
		Error("Chiffre attendu");
	// a digit has been read
	nombre = current -'0';
	ReadChar();
	while(current>='0' && current<='9')
	{
		nombre *= 10; //shift one digit to the left
		nombre += current - '0';
		ReadChar();
	}
	cout << "\tpush $"<<nombre<<endl;
}

void ArithmeticExpression(void);			// Called by Term() and calls Term()
void MultiplicativeExpression(void);

void Factor(void)
{
	if(current=='(')
	{
		ReadChar();
		if(current=='*'||current=='/'||current=='%'||current=='&') 
			MultiplicativeExpression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else 
		if (current>='0' && current <='9')
			Number();
	    else
			Error("'(' ou chiffre attendu");
}

void Term(void){
	Factor();
	if(current=='+'||current=='-'||current=='|')
		ArithmeticExpression();
}

void Expression(void)
{
	ArithmeticExpression();
	if (current=='='||current=='>'||current=='<'||current=='!')
	{
		LookAhead();
		oprel = current;
		RelationalOperator();
		ArithmeticExpression();
		cout <<"\t pop %rax"<<endl;
		cout <<"\t pop %rbx"<<endl;
		cout <<"\t cmpq %rax,%rbx"<<endl;
		switch(oprel)
		{
			case '=':cout<<"\t je Vrai"<<endl;
				break;
			case '<':
				if (lookedAhead == '=') cout<<"\t jbe Vrai"<<endl;
				else cout<<"\t jb Vrai"<<endl;
				break;
			case '>':
				if (lookedAhead == '=') cout<<"\t jae Vrai"<<endl;
				else cout<<"\t ja Vrai"<<endl;
				break;
			case '!':
				if (lookedAhead =='=') cout<<"\t jne Vrai"<<endl;
				break;	
		}
		cout <<"Faux:\t push $0 \t #Faux"<<endl;
		cout <<"\t jmp Suite"<<endl;
		cout <<"Vrai:\t push $1 \t #Vrai"<<endl;
		cout <<"Suite:"<<endl;
	}
}

void ArithmeticExpression(void){
	char adop;
	Term();
	while(current=='+'||current=='-'||current=='|'){
		if (current=='|')
			LookAhead();
		adop=current;
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else if(adop=='-')
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
		else if(adop=='|')
			cout <<"\tor %rbx, %rax"<<endl;
		cout << "\tpush %rax"<<endl;			// store result
	}
}

void MultiplicativeExpression(void){
	char mulop;
	Term();
	while(current=='*'||current=='/'||current=='%'||current=='&'){
		if (current=='&')
			LookAhead();
		mulop=current;
		MultiplicativeOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(mulop=='*')
			cout <<"\timul %rbx, %rax"<<endl;
		else if(mulop=='/')
			cout <<"\tidiv %rbx, %rax"<<endl;
		else if(mulop=='&')
			cout <<"\tand %rbx, %rax"<<endl;
		cout << "\tpush %rax"<<endl;			// store result
	}
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
	Expression();
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





